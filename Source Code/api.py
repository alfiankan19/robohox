from PyQt5.QtWidgets import QApplication , QFileDialog  , QFrame,QComboBox,  QLineEdit , QLabel, QMessageBox, QWidget, QPushButton
from PyQt5.QtCore import *
from PyQt5 import QtGui
from PyQt5 import QtCore
from PyQt5.QtGui import QPixmap
from Result_Window_Final import ResultWindow
from PIL import Image, ImageChops, ImageEnhance
from keras.models import load_model
from pylab import *
import argparse
import os
import cv2
import datetime




def test_image_with_ela(image_path, model_path):
        """
            Error Level Analysis
            :param  image_path
            :return label[Forged , Not Forged] , prob[class probability]
        """
        # loading Model
        model = load_model(model_path)
        # Read image
        image_saved_path = image_path.split('.')[0] + '.saved.jpg'

        # calculate ELA
        image = Image.open(image_path).convert('RGB')
        image.save(image_saved_path, 'JPEG', quality=90)
        saved_image = Image.open(image_saved_path)
        ela = ImageChops.difference(image, saved_image)
        extrema = ela.getextrema()
        max_diff = max([ex[1] for ex in extrema])
        if max_diff == 0:
            max_diff = 1
        scale = 255.0 / max_diff
        ela_im = ImageEnhance.Brightness(ela).enhance(scale)
        ela_im.save("elatemp.jpg","JPEG")

       


        # prepare image for testing
        image = array(ela_im.resize((128, 128))).flatten() / 255.0
        image = image.reshape(-1, 128, 128, 3)
        # prediction
        prob = model.predict(image)[0]
        idx = np.argmax(prob)
        pred = model.predict(image)
        pred = pred.argmax(axis=1)[0]

        label = "Forged" if pred == 1 else "Not_Forged"



        radiuss=41
        image = cv2.imread("elatemp.jpg")
        aslinya = cv2.imread(image_path)
        #cv2.imwrite("gbrasli.jpg",aslinya) 
        orig = image.copy()
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        # perform a naive attempt to find the (x, y) coordinates of
        # the area of the image with the largest intensity value
        (minVal, maxVal, minLoc, maxLoc) = cv2.minMaxLoc(gray)
        cv2.circle(image, maxLoc, 5, (255, 0, 0), 2)
        # display the results of the naive attempt
        cv2.imwrite("hasilakhir_awal.jpg", image) 
        # apply a Gaussian blur to the image then find the brightest
        # region
        gray = cv2.GaussianBlur(gray, (radiuss, radiuss), 0)
        (minVal, maxVal, minLoc, maxLoc) = cv2.minMaxLoc(gray)
        image = orig.copy()

        now = datetime.datetime.now()
        hasilname=now.strftime("%Y%m%d%H%M%S")

        if label=="Forged":
            cv2.circle(aslinya, maxLoc, 100, (255, 0, 0), 5)
            # display the results of our newly improved method
            cv2.imwrite("hasil/"+hasilname+"_hasil.jpg", aslinya) 
        else:
            cv2.imwrite("hasil/"+hasilname+"_hasil.jpg", aslinya) 
        #imagenew = cv2.circle(aslinya, (864, 389), 100, (255, 0, 0), 10) 
        #cv2.imwrite("hasilakhirbaru.jpg", imagenew) 

        cv2.waitKey(0)
        print(minVal, maxVal, minLoc, maxLoc)



        return label, prob[0],prob[1],hasilname+"_hasil.jpg"
        #print(label,prob)

def test_image_with_vgg16(image_path,model_path):
        """
                VGG16 GoogleNet Competition Pre-trained Model
                :param  image_path
                :return label[Forged , Not Forged] , prob[class probability]
        """
        model = load_model(model_path)
        # Read image
        image = Image.open(image_path).convert('RGB')

        # prepare image for testing
        image = array(image.resize((300, 300))).flatten() / 255.0
        image = image.reshape(-1, 300, 300, 3)

        # Make predictions on the input image
        prob = model.predict(image)[0]
        idx = np.argmax(prob)

        # predictions
        prob = model.predict(image)[0]
        idx = np.argmax(prob)
        pred = model.predict(image)
        pred = pred.argmax(axis=1)[0]

        label = "Forged" if pred == 1 else "Not_Forged"
        return label, prob[0],prob[1]
