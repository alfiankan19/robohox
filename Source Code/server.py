from flask import Flask,abort,render_template,request,redirect,url_for
from werkzeug.utils import secure_filename
import os
import api
import datetime
import numpy as np
from flask import send_file
from flask_json import FlaskJSON, JsonError, json_response, as_json
from flask_cors import CORS, cross_origin
app = Flask(__name__)
FlaskJSON(app)

UPLOAD_FOLDER = './uploads'
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER


@app.route('/uploader', methods=['GET', 'POST'])
@cross_origin()
def upload_file():
    if request.method == 'POST':

        if request.method != 'POST':
            return json_response(server_name='ROBOHOX_1', label="",noforged="",forged="",akses="denied")
        else:
            now = datetime.datetime.now()
            tempname=now.strftime("%Y%m%d%H%M%S")

            f = request.files['file']
            filename = secure_filename(f.filename)
            f.save(os.path.join(app.config['UPLOAD_FOLDER'], tempname+filename))

            model = "ELA_Model.h5"
            hasilprediksi = api.test_image_with_ela("uploads/"+tempname+filename, model)

        
            print("Hasil ",type(hasilprediksi[1]))
            print("Hasil ",hasilprediksi[1])

            arr1 = np.array(hasilprediksi[1])
            arr2 = np.array(hasilprediksi[2])

            print(float(arr1),float(arr2))

            return json_response(server_name='ROBOHOX_1', label=hasilprediksi[0],noforged=float(arr1),forged=float(arr2),akses="accepted",image=hasilprediksi[3])
            
@app.route('/hasil',methods=['GET', 'POST'])
@cross_origin()

def hello_hasil():
    fl = request.args.get('file')
    #return fl
    return send_file("hasil/"+fl, mimetype='image/gif')

if __name__ == '__main__':
    app.run(debug=True,host="0.0.0.0",port=8000)