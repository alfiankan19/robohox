from PyQt5.QtWidgets import QApplication , QFileDialog  , QFrame,QComboBox,  QLineEdit , QLabel, QAction, QMessageBox, QWidget, QPushButton
from PyQt5.QtCore import *
from PyQt5 import QtGui
from PyQt5 import QtCore
from PyQt5.QtGui import QPixmap

from pylab import *

import sys
import shutil
import random
import os
import csv

import ELA_Training_Module_Final
import VGG16_Training_Module_Final
import VGG19_Training_Module_Final

#import Test_with_Retraind_Modules

from help_Window import HelpWindow

def train_model(CSV_file , lr , ep, flag = ""):
        if flag == "ELA":
            plot , model = ELA_Training_Module_Final.train_Ela_Model(CSV_file , lr , ep)
            return plot , model
        elif flag == "VGG16":
            plot, model = VGG16_Training_Module_Final.train_VGG16_Model(CSV_file , lr , ep)
            return plot, model
        elif flag == "VGG19":
            plot, model = VGG19_Training_Module_Final.train_VGG19_Model(CSV_file , lr , ep)
            return plot, model

plot , model = train_model("/home/joni/baru/Image-Forgery-Detection-master/Source Code/Error Level Analysis_979664.csv" , 0.1 , 5 , "VGG16")
myThread.start()
